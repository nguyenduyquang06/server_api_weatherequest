# Project WeatherEquest
- Đồ án môn học đại học Bách Khoa 2018. Áp dụng IoT, Machine learning để tính toán, dự đoán mưa
- Kiến trúc microservice
- Server API & Database được triển khai trên Heroku và Google Cloud VPS, client web trên Google Cloud VPS
- Trang web hiện được chay trên google cloud: <a href="http://thutrongtts.ml">http://thutrongtts.ml</a>

# Project planning

<p> 
<img src="https://i1274.photobucket.com/albums/y433/duyquang6/planning_zpsgoneoayw.jpg" />
</p>

# Dataflow

<p> 
<img src="https://i1274.photobucket.com/albums/y433/duyquang6/data_flow_zpsswqvzt01.jpg" />
</p>

# Related project

- <a href="https://github.com/nguyen0096/WeatherDataCollector">Simple website display data and Machine Learning</a>
- <a href="https://gitlab.com/nguyenduyquang06/nodemcu-weather-forecast">Embedded code</a>

