const jwt = require('jsonwebtoken');
'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/sensorsController');

  // todoList Routes
  app.route('/sensors')
    .get(todoList.list_all_records)
    .post(todoList.create_a_record)
    .delete(verifyToken,todoList.delete_all_record);
  
    const freeUser = {
      id: 1, 
      username: 'free',
      password: 'free',
      email: 'tan@gmail.com',
      expiresIn: '30s',
      token: '',
    }
    const premiumUser = {
      id: 2, 
      username: 'premium',
      password: 'pay2go',
      email: 'nguyen0096@gmail.com',
      expiresIn: '5m',
      token:'',
    }

    const admin = {
      id: 3, 
      username: 'admin',
      password: 'adminpw',
      email: 'nguyenduyquang06@gmail.com',
      expiresIn: 0,
      token: '',
    }
    const users = [freeUser,premiumUser,admin]
    app.post('/api/login', (req, res) => {    
    // Mock user  

    var noMatch = true
    
    users.map(user => {
      if (req.body.username==user.username && req.body.password==user.password) {     
        noMatch = false     
        user.token=''
        jwt.sign({user}, 'secretkey', { expiresIn:user.expiresIn }, (err, token) => {
          res.json({
            token
          });
          user.token=token
        });        
      }
    })  
    if (noMatch) {
      res.sendStatus(403);
    }
  });

  function verifyToken(req, res, next) {
    // Get auth header value
    const bearerHeader = req.headers['authorization'];     
    // if (req.headers.host.indexOf('localhost')!=-1){
    //   next()
    // }
       // Check if bearer is undefined
    //else 
    if(typeof bearerHeader !== 'undefined') {
      // Split at the space
      const bearer = bearerHeader.split(' ');      
      // Get token from array
      const bearerToken = bearer[1];      
      // Set the token
      req.token = bearerToken;          
      var promise1 = new Promise(function(resolve, reject) {        
        users.map(user => {             
          if (user.token == bearerToken) {
            resolve(1)
          }        
        })
        resolve(0)        
      }); 
      promise1.then(val => {
        if (val) {
          next()
        } else {
          res.send("invalid token")
        }
      })
      
    } else {
      // Forbidden
      res.sendStatus(403);
    }
  }


  app.route('/sensors/:sensorId')
    .get(todoList.read_a_record)
    .put(todoList.update_a_record)
    .delete(todoList.delete_a_record);
};
