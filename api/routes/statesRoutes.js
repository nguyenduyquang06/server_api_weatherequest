'use strict';
module.exports = function(app) {
  var todoList = require('../controllers/statesController');
    app.route('/states')
    .get(todoList.list_states)
    .put(todoList.update_a_state)
    .delete(todoList.delete_state);

};
