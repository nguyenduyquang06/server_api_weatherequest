'use strict';
var mongoose = require('mongoose'),
  States = mongoose.model('States');

exports.update_a_state = function(req, res) {
  States.findOneAndUpdate({}, req.body, {new: true}, function(err, record) {
    if (err)
      res.send(err);
    res.json(record);
  });
};

exports.list_states = function(req, res) {
  States.findOne({}, function(err, item) {    
    if (err) {          
      res.send(err)
    }      
    else {
      if (!item) {
        var new_Sensor = new States({
          production:'false',
          testMode:'true',
          trainingMode:'false',
          isRaining:'false',
          statusServer:'running',
        });
        new_Sensor.save(function(err, record) {
          if (err)
            res.send(err);          
          res.json(record);
        });
      }
      else {
        res.json(item)    
      }      
    }    
  })
};

exports.delete_state = function(req, res) {
  States.remove({    
  }, function(err, record) {
    if (err)
      res.send(err);
    res.json({ message: ' State successfully deleted' });
  });
};