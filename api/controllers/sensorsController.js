'use strict';
var mongoose = require('mongoose'),
  Sensor = mongoose.model('Sensors');

exports.list_all_records = function(req, res) {
  console.log(req.query);
  var limit = req.query.limit || 10;
  var startTime = parseInt(req.query.starttime) || 0;
  var endTime = parseInt(req.query.endtime) || 0;
  limit = parseInt(limit)
  if (req.query.sort=='desc') {
      if (startTime && endTime) {
        Sensor.find({Created_date: { $gte: startTime, $lte: endTime}}).sort({Created_date: 'desc'}).exec(function(err,record) {
          if (err)
            res.send(err);  
          res.json(record);
          console.log('test2')
        });
      }
      else {
        Sensor.find({}).sort({Created_date: 'desc'}).limit(limit).exec(function(err,record) {
          if (err)
            res.send(err);
          res.json(record);
          console.log('test1')
        }); 
      }

  } else {
    if (startTime && endTime) {
      Sensor.find({Created_date: { $gte: startTime, $lte: endTime}}).sort({Created_date: 'asc'}).exec(function(err,record) {
        if (err)
          res.send(err);  
        res.json(record);
        console.log('test2')
      });
    }
    else {
      Sensor.find({}).limit(parseInt(limit)).exec(function(err, record) {
        if (err)
          res.send(err);
        res.json(record);
      });
    }

  }
};

exports.create_a_record = function(req, res) {
  var new_Sensor = new Sensor(req.body);
  new_Sensor.save(function(err, record) {
    if (err)
      res.send(err);
    res.json(record);
  });
};


exports.read_a_record = function(req, res) {
  Sensor.findById(req.params.sensorId, function(err, record) {
    if (err)
      res.send(err);
    res.json(record);
  });
};


exports.update_a_record = function(req, res) {
  Sensor.findOneAndUpdate({_id: req.params.sensorId}, req.body, {new: true}, function(err, record) {
    if (err)
      res.send(err);
    res.json(record);
  });
};


exports.delete_a_record = function(req, res) {
  Sensor.remove({
    _id: req.params.sensorId
  }, function(err, record) {
    if (err)
      res.send(err);
    res.json({ message: 'Record successfully deleted' });
  });
};

exports.delete_all_record = function(req, res) {
  Sensor.remove({    
  }, function(err, record) {
    if (err)
      res.send(err);
    res.json({ message: ' All records successfully deleted' });
  });
};