'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var SensorSchema = new Schema({
  name: {
    type: String,
    required: 'Kindly enter the name of the task'
  },
  temp: {
    type: Number,
    required:'Require temp field'
  },
  humid: {
    type: Number,
    required:'Require humid field'
  },
  gpsX: {
    type: Number,
    required:'Require gps field'
  },
  gpsY: {
    type: Number,
    required:'Require gps field'
  },
  Created_date: {
    type: Number, 
    default: function(){return new Date().getTime()} 
  },
  isRaining: {
    type: Boolean,
    default: false
  },
  status: {
    type: [{
      type: String,
      enum: ['pending', 'ongoing', 'completed']
    }],
    default: ['pending']
  }
});

module.exports = mongoose.model('Sensors', SensorSchema);