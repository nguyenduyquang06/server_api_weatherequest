'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var StateSchema = new Schema({
  production: {
    type: Boolean,
    required: 'Require production field'
  },
  trainingMode: {
    type: Boolean,
    required:'Require trainingMode field'
  },
  testMode: {
    type: Boolean,
    required:'Require testMode field'
  },
  isRaining: {
    type: Boolean,
    required:'Require isRaining field'
  },
  statusServer: {
    type: [{
      type: String,
      enum: ['running', 'halting', 'stopped']
    }],
    default: ['running']
  }
});

module.exports = mongoose.model('States', StateSchema);