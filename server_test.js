var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000,
  mongoose = require('mongoose'),
  Sensor = require('./api/models/sensorsModel'), //created model loading here
  bodyParser = require('body-parser'),
  mqtt = require('mqtt'),
  request = require('request'),
  cors = require('cors'),
  options = {
      port: 12972,
      host: 'mqtt://m10.cloudmqtt.com',
      clientId: 'mqttjs_' + Math.random().toString(16).substr(2, 8),
      username: 'pszdadvc',
      password: 'eaNOhD6APiQW',
      keepalive: 60,
      reconnectPeriod: 1000,
      protocolId: 'MQIsdp',
      protocolVersion: 3,
      clean: true,
      encoding: 'utf8'
  };
  

app.use(cors())
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/testapi'); 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var routes = require('./api/routes/sensorsRoutes'); //importing route
routes(app); //register the route
app.use(function(req, res) {
    res.status(404).send({url: req.originalUrl + ' not found'})
  });
//app.listen(port);

var server = require('http').Server(app);
var client = mqtt.connect('mqtt://m10.cloudmqtt.com', options);
var io = require('socket.io')(server);
client.on('connect', function() { // When connected
    // subscribe to a topic
    console.log('mqtt connected');
    client.subscribe('data');
    client.on('message', function(topic, msg, packet) {
        console.log("Received '" + msg + "' on '" + topic + "'");
        var res = msg.toString('utf8').split(",");

        request.post(
            'http://localhost:3000/sensors',
            { json: { 
                gpsX: res[0],
                gpsY: res[1],
                humid: res[2],
                temp: res[3],                                        
                name:'test'
                } 
            },
            function (error, response, body) {
                if (!error && response.statusCode == 200) {
                    //console.log(body)
                    console.log(body);
                    io.emit('broadcast',{data_sensor:{ 
                        gpsX: res[0],
                        gpsY: res[1],
                        humid: res[2],
                        temp: res[3],
                        created_time:body.Created_date,
                        name:'test'
                    }});
                }
                else {
                    throw error;
                }
            }
        );
    });
});  
io.on('connection', (socket) => {
    console.log('socket connected')    
    socket.on('reply',(res) => {
        console.log(res);
    });
    socket.on('disconnect', () => {
        console.log('client disconnected');        
    });
});
server.listen(port);
console.log('todo list RESTful API server started on: ' + port);